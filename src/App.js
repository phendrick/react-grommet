import React, {useState} from 'react';

import './App.css';
import { Box, Text, InfiniteScroll, Grid, Grommet } from 'grommet';

const theme = {
  global: {
    colors: {
      brand: '#228BE6',
      error: {
        light: '#222ccc',
        dark: '#ababab'
      }
    },
    font: {
      family: 'Roboto',
      size: '18px',
      height: '20px',
    },
  },
};

const fetchNext = (page, per_page = 5) => {
  console.log("Fetch page", page);
  const count = (parseInt(page, 10) - 1) * per_page;
  if(page === 10) return [];

  return Array(per_page).fill().map((_, i) => `New item ${count+i+1}`);
}

const initialItems = fetchNext(1, 10)

const ScrollingTray = () => {
  const [page, setPage] = useState(1)
  const [items, setItems] = useState(initialItems)

  const onMoreCallback = () => {
    setPage(page + 1)
    const nextItems = fetchNext(page+1, 10)
    const newItems = [...items.slice(), ...nextItems]
    
    setItems(newItems)
  }
  
  return <Box fill overflow={{vertical: 'scroll'}} className='scrollable-area'>
    <InfiniteScroll items={items} onMore={onMoreCallback} step={5}>
      {(item, index) => (
        <Box key={index} pad="xlarge" background={`neutral-${(index % 4) + 1}`} step={5} align="center">
          <Text weight="bold" color="white">
            {item}
          </Text>
        </Box>
      )}
    </InfiniteScroll>
  </Box>;
};

function App() {
  return (
    <Grommet theme={theme}>
      <Grid
        areas={[
          { name: 'main', start: [0, 0], end: [0, 0] },
          { name: 'side', start: [1, 0], end: [2, 0] },
          { name: 'foot', start: [0, 1], end: [2, 1] },
        ]}
        columns={['medium', 'flex', 'medium']}
        rows={['large', 'small']}
        gap='small'>
        <Box gridArea='main' background='brand'>
          <ScrollingTray />
        </Box>

      </Grid>
    </Grommet>
  );
}

export default App;
