This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

# Grommet 
https://v2.grommet.io/ is a mobile-first component library for responsive & accessible React components.

There's a component library here https://v2.grommet.io/components (usable storybook here: https://storybook.grommet.io/?path=/story/all--all) and docs here https://v2.grommet.io/docs

If you step through the commits in this repo, App.js has some basic usage of some of the Grommet components. 

## basic grommet layout
An AppBar (custom component based on a Box component) and flex box row component (Box with a direction property). 
Grommet uses flexbox extensively so this may be a bit of a departure from our usual way of building layouts. 

## fill the available space
Like the basic layout, but this one is wrapped with a <Box> that has the `fill` attribute, so it'll expand to fill all available space. 
This allows the <Collapsible> component to be full-size when it's expanded. 

## responsive collapsible and icons
As above, but with an example of how to use <Icon> components. There's a set of standard icons that we can use, and customise for more bespoke stuff. 

## responsive contexts dont show sidebar at small sizes
The <ResponsiveContext> component allows us to build responsive components really easily. 
ResponsiveContext takes child function with a `size` parameter, which we can then use inline or as a prop to child components. 
Size is one of small/medium/large, but we can customise the breakpoints. 

Using this approach will let us do stuff like: 

```
(size) => {
    <CustomHeroComponent size={size}>...</CustomHeroComponent>
}

const CustomHeroComponent = (props) => {
    <Box className={`hero-image hero-image--size-${size}`}>
        ...
    </Box>
}
```

## infinite scroll
Using Grommets InfiniteScroll and some CSS we can make a scrollable tray. This commit has some css in App.css to target the scroll container. 
We can either use Styled Components (https://styled-components.com/docs) or a more traditional CSS/SCSS approach to this. 

# Base component & template library
If we're going to do away with the old approach to building sites & apps, and build using Gatsby we'll need to build a set of re-usable layouts and components 
that allow us to quickly flesh out the basic layout and structure as quickly as possible. 

Initially, we'll likely add components and style them but common usecases should be abstracted out into a Styled Component (a component which has its own style) 
which can later be customised either by: 
* CSS targetting that component type
* A bespoke Styled Component which uses composition to wrap the base component with new behaviour. 
